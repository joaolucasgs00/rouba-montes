/*
    Caro Ed
    Espero que esta mensagem o encontre bem
    Quero que saiba que enquanto eu escrevia esse código, somente eu e Deus sabíamos como ele funcionava
    Agora, somente Deus sabe
*/

#include <stdio.h>
#include <stdlib.h>

//Verifica o SO e importa a biblioteca correspondente
#ifdef __linux__
    #include <unistd.h>
#elif __WIN32
    #include <Windows.h>
#else

#endif

#define TAMANHO_BARALHO 52
#define NUM_JOGADORES 4

//Criação dos tipos de dados do Jogo
typedef struct carta{
    int numero;
    char naipe;
}Carta;

typedef struct itemPilha{
    Carta *carta;
    struct itemPilha *prox;
}ItemPilha;

typedef struct pilha{
    ItemPilha *topo;
    int tamanho;
}Pilha;

typedef struct itemLista{
    int chave;
    Carta *carta;
    struct itemLista *anterior;
    struct itemLista *proximo;
}ItemLista;

typedef struct lista{
    int tamanho;
    ItemLista *primero;
    ItemLista *ultimo;
}Lista;

typedef struct jogador{
    int num;
    Pilha *monte;
    char nome[30];
}Jogador;
//Fim da Criação dos tipos de dados do Jogo

//Funções de Pilha
Pilha* criaPilha();
int empilha(Carta *c, Pilha *p);
Carta* desempilha(Pilha *p);
int pilhaEstaVazia(Pilha *p);
ItemPilha* criaItemPilha(Carta *carta, ItemPilha *prox);

//Funções de Lista
Lista* criaLista();
ItemLista* criaItemLista(Carta *carta, ItemLista *final);
int ListaEstaVazia(Lista *l);
int insereItemLista(Carta *c, Lista *l);
Carta* removeItemLista(Lista *l, ItemLista *il);

//Funções Mecânicas
void embaralha(Carta *baralho, Pilha *monte);
int numAleatorio(int lim);
Carta* criaBaralho();
void criaJogadores();
void passaTurno();

//Funções de Interface
void mostraMenu();
void limparTela();
void ranking();
void jogar();
void esperar();
char decCarta(Carta *carta);
ItemLista* possivelpegar(int *descarte, int *p1, int *p2, int *p3, int *p4, Carta *carta);

//Variáveis Globais
Pilha *pCompra;
Lista *aDescarte;
int jogando = 0;
int turno = 0;
Jogador *jogadores;

int main(){ 
   mostraMenu();
    return 0;
}

void ranking(){
    limparTela();
    printf("Pressione enter para voltar ao menu...\n");
    esperar();
}

void passaTurno(){
    if(turno != 0){
        if(turno < 4){
            turno++;
        }else{
            turno = 1;
        }
    }else{
        turno++;
    }
}

void mostraMenu(){
    int opc = 100;
    while (opc != 0){
        limparTela();
        printf("Rouba montes\n");
        printf("==========Menu de opções==========\n\n");
        printf("1 - Novo Jogo\n");
        printf("2 - Ranking de jogadores\n");
        printf("0 - Sair\n\n");
        printf("Sua escolha: ");
        scanf("%d", &opc);
        switch (opc){
        case 0:
            printf("Foi bom te ter por aqui...\n");
            break;
        case 1:
            jogando = 1;
            jogar();
            break;
        case 2:
            ranking();
            break;
        
        default:
            limparTela();
            printf("Opção inválida!\n");
            esperar();
            break;
        }
    }
}

void limparTela(){    
    #ifdef __linux__
        system("clear");
    #elif __WIN32
        system("cls");
    #else

    #endif
}

void criaJogadores(){
    jogadores = (Jogador *) malloc(NUM_JOGADORES * sizeof(Jogador));
    for (int i = 0; i < NUM_JOGADORES; i++){
        jogadores[i].num = i + 1;
        jogadores[i].monte = (Pilha *) malloc(sizeof(Pilha));
    }
    turno++;
}

char decCarta(Carta *carta){
    switch (carta->numero){
    case 11:
        return 'J';
        break;
    case 12:
        return 'Q';
        break;
    case 13:
        return 'K';
        break;
    default:
        return carta->numero + '0';
        break;
    }
    
}

void esperar(){
    setbuf(stdin, NULL);
    getchar();
}
void embaralha(Carta *baralho, Pilha *monte){
    for (int i = 0; i < TAMANHO_BARALHO; i++){
		int r = rand() % TAMANHO_BARALHO;
		Carta temp = baralho[i];
		baralho[i] = baralho[r];
		baralho[r] = temp;
	}
    for (int i = 0; i < TAMANHO_BARALHO; i++){
        empilha(&baralho[i], monte);
    }
    
}

int numAleatorio(int lim){
    return rand() % lim;
}

int insereItemLista(Carta *c, Lista *l){
    printf("Tentando inserir\n");
    ItemLista *item = criaItemLista(c, l->ultimo);
    if(ListaEstaVazia(l)){
        l->primero = item;
        item->chave = 0;
    }else{
        item->chave = l->ultimo->chave + 1;
    }
    l->ultimo = item;
    l->tamanho += 1;

    if(l->ultimo == item){
        return 1;
    }else{
        return 0;
    }
}

Carta* removeItemLista(Lista *l, ItemLista *il){
    if (!ListaEstaVazia(l) && il != NULL){
        Carta *retorno; 
        ItemLista *atual = l->primero;
        while (atual != NULL) {
            if(atual == il){
                retorno = atual->carta;
                if(atual != l->primero){
                    atual->anterior->proximo = atual->proximo;
                }else{
                    l->primero = atual->proximo;
                }

                if(atual != l->ultimo){
                    atual->proximo->anterior = atual->anterior;
                }else{
                    l->ultimo = atual->anterior;
                }
                free(atual);
                break;
            }
            atual = atual->proximo;
        }
        l->tamanho--;
        return retorno;
    }else{
        return NULL;
    }
}

Carta* desempilha(Pilha *p){
    if (!pilhaEstaVazia(p)){
        Carta *carta = p->topo->carta;
        ItemPilha *itemLiberar = p->topo;    
        p->topo = p->topo->prox;
        free(itemLiberar);
        p->tamanho--;
        return carta;
    }else{
        return NULL;
    }
}

int empilha(Carta *c, Pilha *p){
    ItemPilha *item = criaItemPilha(c, p->topo);
    p->topo = item;
    if (pilhaEstaVazia(p)){
        p->tamanho = 1;
    }else{
        p->tamanho++;
    }
    
    if(p->topo == item){
        return 1;
    }else{
        return 0;
    }
}

Pilha* criaPilha(){
    return (Pilha *) malloc(sizeof(Pilha));
}

Lista* criaLista(){
    return (Lista *) malloc(sizeof(Lista));
}

ItemLista* criaItemLista(Carta *carta, ItemLista *final){
    ItemLista *item = (ItemLista *) malloc(sizeof(ItemLista));
    if(final == NULL){
        item->chave = 0;
        item->anterior = NULL;
        item->proximo = NULL;
    }else{
        item->chave = final->chave + 1;
        item->anterior = final;
        item->anterior->proximo = item;
    }
    item->proximo = NULL;
    item->carta = carta;
    return item;
}

ItemPilha* criaItemPilha(Carta *carta, ItemPilha *prox){
    ItemPilha *item = (ItemPilha *) malloc(sizeof(ItemPilha));
    item->carta = carta;
    item->prox = prox;
    return item;
}

int pilhaEstaVazia(Pilha *p){
    if(p->topo == NULL){
        return 1;
    }else{
        return 0;
    }
}

int ListaEstaVazia(Lista *l){
    if(l->primero == NULL){
        return 1;
    }else{
        return 0;
    }
}

Carta* criaBaralho(){
    Carta *baralho = (Carta *) malloc(52 * sizeof(Carta));
    int i;
    char naipe = 'c';
    int c = 0;
        for(i = 0; i < 13; i++){
            baralho[c].numero = i + 1;
            baralho[c].naipe = naipe;
            c++;
        }
        naipe = 'e';
        for(i = 0; i < 13; i++){
            baralho[c].numero = i + 1;
            baralho[c].naipe = naipe;
            c++;
        }
        naipe = 'o';
        for(i = 0; i < 13; i++){
            baralho[c].numero = i + 1;
            baralho[c].naipe = naipe;
            c++;
        }
        naipe = 'p';
        for(i = 0; i < 13; i++){
            baralho[c].numero = i + 1;
            baralho[c].naipe = naipe;
            c++;
        }
    return baralho;
}

ItemLista* possivelpegar(int *descarte, int *p1, int *p2, int *p3, int *p4, Carta *carta){
    ItemLista *atual = aDescarte->primero;
    while (atual != NULL) {
        if(atual->carta->numero == carta->numero){
            *descarte = 1;
            return atual;
        }
        atual = atual->proximo;
    }

    for (int i = 0; i < NUM_JOGADORES; i++){
        if (!pilhaEstaVazia(jogadores[i].monte)){     
            ItemPilha *ip = jogadores[i].monte->topo;
            while (ip != NULL){
                if (ip->carta->numero == carta->numero){
                    switch (i){
                    case 1:
                        *p1 = 1;
                        break;
                    case 2:
                        *p2 = 1;
                        break;
                    case 3:
                        *p3 = 1;
                        break;
                    case 4:
                        *p4 = 1;
                        break;
                    default:
                        break;
                    }
                }
                ip = ip->prox;
            }
        
        }else{
            printf("Está vazia \n");
        }
    }
    return NULL;
}

void jogar(){
    Carta *baralho = criaBaralho();
    pCompra = criaPilha();
    aDescarte = criaLista();
    embaralha(baralho, pCompra);
    criaJogadores();
    while (jogando){

        limparTela();
        
        if (ListaEstaVazia(aDescarte)){
            printf("");
            for (int i = 0; i < 5; i++){
                insereItemLista(desempilha(pCompra), aDescarte);
            } 
        }

        printf("Turno do jogador %d - Cartas no monte: %d\n\n\n\n",turno ,jogadores[turno - 1].monte->tamanho);
        
        ItemLista *atual = aDescarte->primero;
        printf("Área de Descarte(%d): \n", aDescarte->tamanho);
        while (atual != NULL) {
            printf("%c%c  - %x\n", decCarta(atual->carta), atual->carta->naipe, atual);
            atual = atual->proximo;
        }
        printf("\n\n\n\n");

        printf("Baralho(%d)\n\n\n\n", pCompra->tamanho);

        for (int i = 0; i < NUM_JOGADORES; i++){
            if (/*jogadores[i].num != turno*/ 1){
                char simb = '0';
                char np = '0';
                if(!pilhaEstaVazia(jogadores[i].monte)){
                    simb = decCarta(jogadores[i].monte->topo->carta);
                    np = jogadores[i].monte->topo->carta->naipe;
                }
                printf("Monte do jogador %d(%d): %c%c \n", jogadores[i].num, jogadores[i].monte->tamanho, simb, np);
            }            
        }
        
        printf("Pressione enter para comprar uma carta");
        esperar();
        Carta *cartaVez = desempilha(pCompra);
        printf(" - Carta da Vez: %c%c\n", decCarta(cartaVez), cartaVez->naipe);
        printf("indo verificar se é possível \n");
        int p1 = 0, p2 = 0, p3 = 0, p4 = 0, descart = 0;
        ItemLista *il = possivelpegar(&descart, &p1, &p2, &p3, &p4, cartaVez);
        if (il == NULL){
            printf("Nulo ao quadrado\n");
        }
        
        printf("Verificado \n");
        int quantp = (p1 + p2 + p3 + p4 + descart);
        switch (quantp){
        case 0:
            printf("Não é possível pegar nenhuma carta\n");
            insereItemLista(cartaVez, aDescarte);
            break;
        case 1:
            if (p1 == 1 && turno != 1){
                int tamMonte1 = jogadores[0].monte->tamanho;
                Carta *c[tamMonte1];
                for (int i = 0; i < tamMonte1; i++){
                    c[i] = desempilha(jogadores[0].monte);
                }
                for (int i = tamMonte1 - 1; i >= 0; i--){
                    empilha(c[i], jogadores[0].monte);
                }                
            }else if (p2 == 1 && turno != 2){
                int tamMonte1 = jogadores[1].monte->tamanho;
                Carta *c[tamMonte1];
                for (int i = 0; i < tamMonte1; i++){
                    c[i] = desempilha(jogadores[1].monte);
                }
                for (int i = tamMonte1 - 1; i >= 0; i--){
                    empilha(c[i], jogadores[1].monte);
                }  
            }else if (p3 == 1 && turno != 3){
                int tamMonte1 = jogadores[2].monte->tamanho;
                Carta *c[tamMonte1];
                for (int i = 2; i < tamMonte1; i++){
                    c[i] = desempilha(jogadores[2].monte);
                }
                for (int i = tamMonte1 - 1; i >= 0; i--){
                    empilha(c[i], jogadores[2].monte);
                }  
            }else if (p4 == 1 && turno != 4){
                int tamMonte1 = jogadores[3].monte->tamanho;
                Carta *c[tamMonte1];
                for (int i = 0; i < tamMonte1; i++){
                    c[i] = desempilha(jogadores[3].monte);
                }
                for (int i = tamMonte1 - 1; i >= 0; i--){
                    empilha(c[i], jogadores[3].monte);
                }  
            }else{
                printf("Vamos pegar da área de descarte\n");
                empilha(removeItemLista(aDescarte, il), jogadores[turno - 1].monte);
                empilha(cartaVez, jogadores[turno - 1].monte);           
            }
        default:
            break;
        }

        esperar();
        passaTurno();
    }
}