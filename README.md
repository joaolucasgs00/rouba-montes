# Trabalho Prático 1 - Algoritmos e Estruturas de Dados II

Este é o Trabalho Prático 1 da disciplina de Algoritmos e Estruturas de Dados II. O objetivo deste trabalho é implementar o jogo Rouba Montes utilizando a linguagem de programação C, aplicando os conceitos aprendidos ao longo do semestre.

## Descrição do Problema - Rouba Montes

O Rouba Montes é um jogo de cartas simples e divertido para crianças. Utiliza um ou mais baralhos normais, onde as cartas são distinguidas apenas pelo valor, sem considerar os naipes.

### Regras do Jogo

1. **Setup Inicial:**
   - Embaralhar as cartas e formar a pilha de compra com as cartas viradas para baixo.
   - Cada jogador começa sem cartas em seu monte.

2. **Desenvolvimento do Jogo:**
   - Os jogadores jogam em sequência, no sentido horário.
   - Em sua vez, o jogador:
     - Retira a carta de cima da pilha de compra.
     - Se a carta da vez for:
       - Igual a alguma carta presente na área de descarte, o jogador retira essa carta da área de descarte, coloca-a no topo de seu monte e continua sua vez.
       - Igual à carta de cima de outro monte, o jogador "rouba" esse monte, colocando-o no seu próprio monte e continua sua vez.
       - Igual à carta de cima do seu próprio monte, coloca a carta da vez no topo de seu monte e continua sua vez.
       - Diferente das cartas da área de descarte e dos topos dos montes, coloca a carta na área de descarte e encerra sua jogada.
   
3. **Final do Jogo:**
   - O jogo termina quando não há mais cartas na pilha de compra.
   - O jogador com o maior monte (maior número de cartas) vence. Em caso de empate, todos com o maior monte vencem.

## Como Executar

1. Compile o código-fonte em C.
2. Execute o programa gerado.

## Contribuição

Contribuições são bem-vindas! Sinta-se à vontade para propor melhorias ou correções neste projeto.

## Autor

João Lucas Gonçalves - joao.1686177@discente.uemg.br

---

Universidade do Estado de Minas Gerais  
Professor: Edwaldo Soares Rodrigues  
Disciplina: AEDS II
